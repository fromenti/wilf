#include "semigroup.hpp"

#define STACK_SIZE (MAX_GENUS*(MAX_GENUS+1))/2

class Stack{
public:
  Semigroup data[STACK_SIZE+1], *stack[STACK_SIZE], *other;
  size_t stack_size;
  Stack();
  Semigroup* pop();
  Semigroup* pushed();
  bool is_empty();
};


inline
Stack::Stack(){
  for (ind_t i=0; i<STACK_SIZE; i++) stack[i] = &(data[i]);
  other=&data[STACK_SIZE];
  stack_size=0;
}

inline Semigroup*
Stack::pop(){
  swap(stack[--stack_size],other);
  return other;
}

inline Semigroup*
Stack::pushed(){
  return stack[stack_size++];
};

inline bool
Stack::is_empty(){
  return stack_size==0;
}
