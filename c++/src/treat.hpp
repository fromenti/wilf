

#ifndef TREAT_HPP
#define TREAT_HPP

#include "semigroup.hpp"
#include "results.hpp"
#include "cuts.hpp"

void treat(const Semigroup& S,Results& res);
  
inline void treat(const Semigroup& S,Results& res){
  //cout<<&res<<endl;	
  res.n1[S.genus]++;

    if(is_special(S)){
      res.n2[S.genus]++;
    }

  
}

#endif
