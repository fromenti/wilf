#ifndef RESULTS_CILK
#define RESULTS_CILK

#include <cilk/cilk.h>
#include <cilk/reducer.h>

#include "results.hpp"

class ResultsReducer{
private:
  struct Monoid:cilk::monoid_base<Results>{
    static void reduce(Results* left,Results* right);
  };

  cilk::reducer<Monoid> imp_;
public:
  ResultsReducer();
  void reset();
  Results& get_results();
};

extern ResultsReducer cilk_results;

inline
ResultsReducer::ResultsReducer():imp_(){
}
inline void
ResultsReducer::reset(){
  imp_.view().clear();
}

inline void
ResultsReducer::Monoid::reduce(Results* left,Results* right){
  left->add(*right);
}

inline Results&
ResultsReducer::get_results(){
  return imp_.view();
}
#endif

