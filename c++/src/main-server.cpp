#include <set>
#include "config.hpp"
#include "../dist/server.hpp"
#include "gtask.hpp"
#include "treewalk.hpp"
#include <algorithm>
#include <list>

using namespace std;

void  init_parallelize(list<Semigroup>& forest,Results& res){
  Semigroup O;
  init_full_N(O);
  for(size_t g=0;g<MAX_GENUS;++g){
    //Treat ordinary semigroup O
    if(not cut(O)){
      treat(O,res);
      auto it=generator_iter<CHILDREN>(O);
      ind_t pos=0;
      it.move_next();
      Semigroup Onext=remove_generator(O,it.get_gen(),pos++);
      while(it.move_next()){
	Semigroup son=remove_generator(O,it.get_gen(),pos++);
	if(not cut(son)){
	  treat(son,res);
	  auto it2=generator_iter<CHILDREN>(son);
	  ind_t pos2=0;
	  while(it2.move_next()){
	    forest.push_back(remove_generator(son,it2.get_gen(),pos2++));
	  }
	}
      }
      O=Onext;
    }
  }
  if(not cut(O)) treat(O,res);
}

int main(int argc,char** argv){
  Results res;
  res.clear();
  list<Semigroup> forest;
  init_parallelize(forest,res);
  // Set tasks
  size_t nb_tasks=forest.size();
  Task* tasks=new Task[nb_tasks];
  size_t ind=0;
  for(auto it=forest.begin();it!=forest.end();++it){
    GTaskInput input;
    input.S=*it;
    tasks[ind].set_input((char*)&input,sizeof(input));
    tasks[ind].set_statut(Task::Unaffected);
    ++ind;
  }

  int l=1;
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd<0){
    cerr<<"[Error] Can not open socket"<<endl;
    exit(-1);
  }
  cout<<"*****************"<<endl;
  cout<<"* Wilf - Server *"<<endl;
  cout<<"*****************"<<endl;
  
  cout<<endl;
  cout<<"----------------- [Server info] ----------------"<<endl;
  Server server(MAX_CLIENTS,SERVER_PORT);
  cout<<"------------------------------------------------"<<endl;
  cout<<"Max genus =  "<<MAX_GENUS<<endl;
  cout<<"Number of tasks : "<<nb_tasks<<endl;
  server.set_tasks(tasks,nb_tasks);
  do{
    server.listen_for_new_clients();
    server.listen_clients();
    server.treat_messages();
    server.affect_tasks();
    usleep(10000);
  }while(server.has_unfinished_tasks());
  size_t number=0;
  cout<<"***************"<<endl;
  cout<<"*    Bilan    *"<<endl;
  cout<<"***************"<<endl;

  for(size_t i=0;i<nb_tasks;++i){
    if(tasks[i].get_statut()==Task::Done){
      GTaskOutput& output=*((GTaskOutput*)tasks[i].get_output());
      res.add(output.res);
      if(res.has_counter_example){
	cout<<"A counter example was found : "<<endl;
	print_Semigroup_gen(res.S_counter_example);
	break;
      }
    }
  }
  for(size_t g=0;g<=MAX_GENUS;++g){
    cout<<g<<","<<res.n1[g]<<","<<res.n2[g]<<endl;
  }
}
