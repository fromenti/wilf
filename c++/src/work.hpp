#ifndef WORK_HPP
#define WORK_HPP

#include <iostream>
#include "treewalk.hpp"

using namespace std;

void work_on(Semigroup& S,Results& res){
	  Stack stack;
   if(not cut(S)){
print_Semigroup(S);
    treat(S,res);
    Semigroup* root=stack.pushed();
cout<<"root = "<<root<<endl;
    *root=S;
    walk(stack,res);
  }
}


#endif
