#include <set>
#include <stack>
#include <fstream>
#include <cstdlib>
#include <chrono>
#include "../dist/worker.hpp"
#include "gtask.hpp"
#include "treewalk.hpp"
#include "work.hpp"

using namespace std;
/*void test_conjecture(size_t offset,GTaskOutput& output){
  output.number=0;
  uint64_t end=(1L<<(n-1));
  for(size_t u=umin+offset;u<end;u+=split){
    Skew s(u);
    if(s.test_conjecture()){
	cout<<u<<endl;
      if(output.number==0) output.sample=u;
      ++output.number;
    }
  }
  }*/

int main(int argc,char** argv){
  Worker worker(SERVER_IP,SERVER_PORT);
  while(true){
    Task task=worker.get_task();
    const GTaskInput* input=(const GTaskInput*)task.get_input();
    Semigroup S=input->S;
    //cout<<"S is "<<endl;
    //print_Semigroup(S);
    GTaskOutput output;
    output.res.clear();
    work_on(S,output.res);
    //test_conjecture(offset,output);
    task.set_output((char*)&output,sizeof(output));
    //cout<<"Send output"<<endl;
    worker.send_task(task);
  }
}
 
