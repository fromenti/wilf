#ifndef TREEWALK_HPP
#define TREEWALK_HPP
#include "semigroup.hpp"
#include "stack.hpp"
#include "treat.hpp"
#include "cuts.hpp"

//bool cut(const Semigroup& m);
//void treat(const Semigroup& m);
void walk(Stack& stack,Results& res);
void signal_checkpoint_handler(int signum);

/*inline bool
cut(const Semigroup& m){
  if(3*m.left_primitive>=m.min) return true;
  return false;
}

inline bool is_special(const Semigroup& S){
  ind_t c=S.conductor;
  ind_t m=S.min;
  ind_t x=c+(m-1)-c%m;
  return S.decs[x]==1;
}*/
#endif
