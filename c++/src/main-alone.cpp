#include "config.hpp"
#include "treewalk.hpp"
#include "work.hpp"
#include <iostream>

using namespace std;

int main(int argc,char** argv){
  Semigroup S;
  Results res;
  res.clear();
  init_full_N(S);
  work_on(S,res);
  if(res.has_counter_example){
    cout<<"A counter example was found : "<<endl;
    print_Semigroup_gen(res.S_counter_example);
  }
  for(size_t g=0;g<=MAX_GENUS;++g){
    cout<<g<<","<<res.n1[g]<<","<<res.n2[g]<<endl;
  }
}
