#ifndef RESULTS_HPP
#define RESULTS_HPP

#include <iostream>
#include "semigroup.hpp"

using namespace std;

class Results{
public:
  size_t n1[MAX_GENUS+1];
  size_t n2[MAX_GENUS+1];

  bool has_counter_example;
  Semigroup S_counter_example;

  void clear();
  void add(const Results& res);
};


inline
void Results::clear(){
  has_counter_example=false;
  for(size_t g=0;g<=MAX_GENUS;++g){
    n1[g]=0;n2[g]=0;
  }
}

inline
void Results::add(const Results& res){
  if((not has_counter_example) and res.has_counter_example){
    S_counter_example=res.S_counter_example;
  }
  for(size_t g=0;g<=MAX_GENUS;++g){
    n1[g]+=res.n1[g];
    n2[g]+=res.n2[g];
  }
}

#endif
