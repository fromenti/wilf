#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>
#include <cpuid.h>
#include <fstream>
#include <csignal>
#include <unistd.h>

#include "treewalk.hpp"

using namespace std;

/*void treat(const Semigroup& m){
  int q=ceil(float(m.conductor)/float(m.min));
  unsigned int rho=q*m.min-m.conductor;
  if(m.wilf<=rho){
    if((m.wilf<0) or (m.wilf<=rho and m.e>2 and m.left_primitive>1 and q>=4)){
      //output(m,file_out);
    }
  }
  }*/



void walk(Stack& stack,Results& res){
  Semigroup *current,*son;
  Semigroup temp;
  while(not stack.is_empty()){
    current = stack.pop();
    size_t g=current->genus;
    if(g<MAX_GENUS-1){
      auto it=generator_iter<CHILDREN>(*current);
      ind_t pos=0;
      while(it.move_next()){
	remove_generator(temp, *current, it.get_gen(),pos++);
	if(not cut(temp)){
	  treat(temp,res);
	  son=stack.pushed();
	  *son=temp;
	}
      }
    }
    else{
      auto it = generator_iter<CHILDREN>(*current);
      ind_t pos=0;
      while(it.move_next()){
	remove_generator(temp, *current, it.get_gen(),pos++);
	if(not cut(temp)){
	  treat(temp,res);
	}
      }
    }
  }
}
