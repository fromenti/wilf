#ifndef CUT_HPP
#define CUT_HPP

#include "semigroup.hpp"
bool cut(const Semigroup& S);
bool cut_wilf_left_primitives(const Semigroup& S);
bool is_special(const Semigroup& S);
bool wilf1(const Semigroup& S);
bool wilf2(const Semigroup& S);
bool wilf3(const Semigroup& S);

inline bool cut(const Semigroup& S){
  //return false;
  return (not wilf1(S)) or (not wilf2(S));
  // return !is_special(S);
}

inline bool is_special(const Semigroup& S){
  ind_t c=S.conductor;
  ind_t m=S.min;
  ind_t x=c+(m-1)-c%m;
  return S.decs[x]==1;
}

inline bool wilf1(const Semigroup& S){
  return K_MANUEL*S.left_primitive<S.min;
}

inline bool wilf2(const Semigroup& S){
  return (K_MANUEL*S.e)<S.min+K_MANUEL*(MAX_GENUS-S.genus);
}

/*inline bool wilf3(const Semigroup& S){
  return 5*S.min<3*(MAX_GENUS+1);
}*/
#endif
