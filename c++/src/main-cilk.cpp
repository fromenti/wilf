#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

#include <iostream>
#include "work.hpp"
#include "results_cilk.hpp"
using namespace std;

void cilk_walk(Semigroup S){
  Semigroup temp;
  if(S.genus<MAX_GENUS-STACK_BOUND){
    treat(S,cilk_results.get_results());
    auto it = generator_iter<CHILDREN>(S);
    ind_t pos=0;
    while (it.move_next()){
      remove_generator(temp,S,it.get_gen(),pos++);
      if(not cut(temp)){
	cilk_spawn cilk_walk(temp);
      }
    }
    //cilk_sync;
  }
  else
    work_on(S,cilk_results.get_results());
}

int main(){
  __cilkrts_set_param("nworkers", CILK_WORKERS);
  cout<<"Cilk workers : "<<__cilkrts_get_nworkers()<<endl;
  Semigroup N;
  init_full_N(N);
  cilk_walk(N);
 
  for (size_t g=0; g<=MAX_GENUS; g++){
    cout << g << "," <<cilk_results.get_results().n1[g] 
	<< "," <<cilk_results.get_results().n2[g]<<endl;
  }
  return 0;
};
