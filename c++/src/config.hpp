#ifndef CONFIG_HPP
#define CONFIG_HPP
#include <cstddef>

using namespace std;
//#define DATA_DIR string("/home/fromentin/data/") //string("/workdir/lmpa/jfromentin/")
#define SERVER_IP "127.0.0.1" //"10.1.0.104" //"192.168.1.3" //Orval 04
#define SERVER_PORT 55555
#define MAX_MSG_SIZE 65536
#define MAX_CLIENTS 2048
#define MAX_WORKERS 2048

#define MAX_GENUS 80
#define STACK_BOUND 20
#define CILK_WORKERS "2"
#define K_MANUEL 4


#endif
