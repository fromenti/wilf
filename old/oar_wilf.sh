#!/bin/sh
#OAR -l core=1,walltime=1:00:00
#OAR -t idempotent
#OAR -q besteffort
#OAR -t besteffort
#OAR --array-param-file oar/param
#OAR -O oar/output/j%jobid%
#OAR -E oar/error/j%jobid%
cd /nfs/home/lmpa/jfromentin/wilf
./wilf tasks/$*
mv tasks/$* tasks/donemv tasks/$*-out tasks/out