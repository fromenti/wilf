#include "semigroup.hpp"
#include "stack.hpp"

bool cut(const Semigroup& m);
void treat(const Semigroup& m);
void walk(Stack& stack);
void signal_checkpoint_handler(int signum);

inline bool
cut(const Semigroup& m){
  if(3*m.left_primitive>=m.min) return true;
  return false;
}
