import sys
import os

if __name__=="__main__":
    if len(sys.argv)!=2:
        print("Usage : python3 "+sys.argv[0]+" genus")
        exit(-1)
    g=int(sys.argv[1])
    mmax=int((3*(g+2))/5)
    n=0
    os.system("make clean;make MAX_GENUS="+repr(g))
    file=open("output/param_"+repr(g),'w')
    for m in range(2,mmax+1):
        for k in range(g-m+1):
            file.write(repr(m)+' '+repr(k)+"\n")
            n+=1
    file.close()
    print("Split in ",n," subtasks")
    file=open("oar_wilf.sh",'w')
    file.write("#!/bin/sh\n")
    file.write("#OAR -l core=1,walltime=100:00:00\n")
    file.write("#OAR -t idempotent\n")
    file.write("#OAR -q besteffort\n")
    file.write("#OAR -t besteffort\n")
    file.write("#OAR --array-param-file output/param_"+repr(g)+"\n")
    file.write("#OAR -O oar/output/job_%jobid%\n")
    file.write("#OAR -E oar/error/job_%jobid%\n")
    file.write("cd /nfs/home/lmpa/jfromentin/wilf/single\n")
    file.write("./wilf $* output/$OAR_JOBID\n")
    file.write("mv output/$OAR_JOBID*wilf* output/done/\n")
    file.close()
    
